import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { OfficeComponent } from "./office/office.component";
import { RequestService } from  "./service/request.service";
import { HttpClientModule } from '@angular/common/http';
import { Office } from './interfaces/office.interfaces';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {CommonModule} from '@angular/common';



@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.css',
    imports: [CommonModule,RouterOutlet, OfficeComponent, HttpClientModule, MatFormFieldModule, MatInputModule, MatDatepickerModule],
})

export class AppComponent implements OnInit {
  constructor(private requestService: RequestService){
  }

  ngOnInit(): void {
    console.log('Inicio del proyecto');
    //this.getPost();
  }
  title = 'office';
  city ='Barcelona';

  getPost(){
    //fetch('http://localhost:8080/clinic/service/Saludo/saludar')
    fetch('http://localhost:8080/clinic/service/ConsultaPersona/consultarPersona')
    .then((response) => response.json())
    .then((json) => console.log(json));
  }


  getAll(){
    this.requestService.getOffice().subscribe({
      next: (response) =>{console.log(response)}
    });
  }

  createOfficeWithAngular(){
    let office ={
      "name": "401",
      "status": "DIS",
      "floor": 4,
      "accessibility": true,
      "description": "Hola Cape"
    };
    this.requestService.createOffice(office).subscribe({
      next: (response) => {console.log(response)},
      error:(error) => {console.log(error)}
    });
  }

  updateOfficeWhitAngular(){
    let office ={
      "name": "401",
      "status": "DIS",
      "floor": 4,
      "accessibility": true,
      "description": "Hola Anyelita"
    };
    this.requestService.updateOffice(office).subscribe({
      next: (response) => {console.log(response)},
      error:(error) => {console.log(error)}
    });
  }
}
