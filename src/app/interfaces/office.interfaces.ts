export interface Office{
    name: string;
    status:string;
    floor: number;
    accessibility: boolean;
    description: string;
    
}