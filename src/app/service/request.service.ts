import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Office } from '../interfaces/office.interfaces';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private httpClient: HttpClient)
  { 
    
  }
  getOffice(){
    //return this.httpClient.get('http://localhost:8080/clinic/service/ConsultaPersona/consultarPersona');
    return this.httpClient.get('http://localhost:8093/office');
  }
  createOffice(office: Office){
    return this.httpClient.post("http://localhost:8093/office",office);
  }

  updateOffice(office: Office){
    return this.httpClient.put("http://localhost:8093/office/${office.id}",office);
  }
}
